export interface HeaderProps {
    className?: string;
    style?: React.CSSProperties;
    title?: string;
}