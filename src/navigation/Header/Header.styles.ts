import { Link } from "react-router-dom";
import { css, styled } from "styled-components";
import { Row } from "../../designSystem/Grid";

export const HeaderRoot = styled(Row)(
    ({ theme }) => css`
        justify-content: space-between;
        align-items: center;
        padding: ${theme.spacing["1"]} ${theme.spacing["4"]};
        border-bottom: 1px solid ${theme.palette.border};
        width: 100%;
        flex-direction: row;
    `,
);

export const HeaderLink = styled(Link)(
    () => css`
        text-decoration: none;
    `,
);