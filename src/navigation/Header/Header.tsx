import Typography from "../../components/Typography/Typography";
import { HeaderProps } from "./Header.types";
import clsx from "clsx";
import { HeaderLink, HeaderRoot } from "./Header.styles";
import { MainRoutes } from "../../router/MainRouter.types";
import { useLoadingStore } from "../../store/loadingStore";
import Spinner from "../../designSystem/Spinner";

function Header({ className, title, ...rest }: HeaderProps) {
  const isLoading = useLoadingStore((state) => state.isLoading);
  return (
    <HeaderRoot className={clsx("header", className)} {...rest}>
      <HeaderLink to={MainRoutes.MAIN}>
        <Typography variant="h2">{title}</Typography>
      </HeaderLink>
      {isLoading && <Spinner />}
    </HeaderRoot>
  );
}

export default Header;
