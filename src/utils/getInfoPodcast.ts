import { useEffect, useState } from 'react';

interface Podcast {
  [key: string]: any;
}

const getInfoPodcast = (podcast: Podcast | undefined): [string, string, string,string,string] => {
  const [image, setImage] = useState<string>('');
  const [title, setTitle] = useState<string>('');
  const [author, setAuthor] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [id, setId] = useState<string>('');

  useEffect(() => {
    if (podcast) {
        if (podcast["im:image"] && podcast["im:image"].length > 0) {
        setImage(podcast["im:image"][podcast["im:image"].length - 1]?.label || '');
        }
        if (podcast["im:name"]) {
        setTitle(podcast["im:name"].label || '');
        }
        if (podcast["im:artist"]) {
        setAuthor(podcast["im:artist"].label || '');
        }
        if (podcast.summary) {
            setDescription(podcast.summary.label || '');
        }
        if (podcast.id) {
            setId(podcast.id.attributes["im:id"] || '');
        }
    } else {
        setImage('');
        setTitle('');
        setAuthor('');
        setDescription('');
        setId('');
    }
  }, [podcast]);

  return [image, title, author, description,id];
};

export default getInfoPodcast;
