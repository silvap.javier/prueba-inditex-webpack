import { Result } from "../types/episodes.types";
import moment from 'moment';

export interface EpisodeInfo {
    trackName: string;
    releaseDate: string;
    trackTimeMillis: string;
    id: number;
}

export const getRowsEpisodesList = (rows: Result[]): EpisodeInfo[] => {
    return rows
        .filter(row => row.wrapperType === "podcastEpisode")
        .map((row) => ({
            trackName: row.trackName,
            releaseDate: moment(row.releaseDate).format('DD/MM/YYYY'),
            trackTimeMillis: moment.utc(row.trackTimeMillis).format('HH:mm:ss'),
            id: row.trackId
        }));
};