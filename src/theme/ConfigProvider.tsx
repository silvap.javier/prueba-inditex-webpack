import { ReactNode } from "react";
import { ThemeProvider as StyledThemeProvider } from "styled-components";
import theme from "./theme";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import React from "react";

export interface ConfigProviderProps {
    children?: ReactNode;
}

const ConfigProvider = ({ children }: ConfigProviderProps): JSX.Element => {
    const queryClient = new QueryClient({
        defaultOptions: {
            queries: {
                retry: false,
                refetchOnWindowFocus: false,
                gcTime: 24 * 60 * 60 * 1000, // 24 horas
                staleTime: Infinity,
            },
        },
    });

    return (
        <StyledThemeProvider theme={theme}>
                <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
        </StyledThemeProvider>
    );
};

export default ConfigProvider;
