export interface EpisodesApiResponse {
    resultCount: number;
    results:     Result[];
}

export interface Result {
    wrapperType:             WrapperType;
    kind:                    Kind;
    artistId?:               number;
    collectionId:            number;
    trackId:                 number;
    artistName?:             string;
    collectionName:          Name;
    trackName:               string;
    collectionCensoredName?: Name;
    trackCensoredName?:      Name;
    artistViewUrl:           string;
    collectionViewUrl:       string;
    feedUrl:                 string;
    trackViewUrl:            string;
    artworkUrl30?:           string;
    artworkUrl60:            string;
    artworkUrl100?:          string;
    collectionPrice?:        number;
    trackPrice?:             number;
    collectionHdPrice?:      number;
    releaseDate:             Date;
    collectionExplicitness?: string;
    trackExplicitness?:      string;
    trackCount?:             number;
    trackTimeMillis:         number;
    country:                 Country;
    currency?:               string;
    primaryGenreName?:       PrimaryGenreNameEnum;
    contentAdvisoryRating:   ContentAdvisoryRating;
    artworkUrl600:           string;
    genreIds?:               string[];
    genres:                  Array<GenreClass | string>;
    episodeFileExtension?:   EpisodeFileExtension;
    artworkUrl160?:          string;
    episodeContentType?:     EpisodeContentType;
    closedCaptioning?:       ClosedCaptioning;
    shortDescription?:       string;
    artistIds?:              number[];
    previewUrl?:             string;
    episodeUrl?:             string;
    episodeGuid?:            string;
    description?:            string;
}

export type ClosedCaptioning = "none";

export type Name = "The Joe Budden Podcast";

export type ContentAdvisoryRating = "Explicit" | "Clean";

export type Country = "USA";

export type EpisodeContentType = "audio";

export type EpisodeFileExtension = "mp3";

export interface GenreClass {
    name: PrimaryGenreNameEnum;
    id:   string;
}

export type PrimaryGenreNameEnum = "Music";

export type Kind = "podcast" | "podcast-episode";

export type WrapperType = "track" | "podcastEpisode";
