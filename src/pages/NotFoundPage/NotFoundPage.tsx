import { Link } from "react-router-dom";
import {
  NotFound404Text,
  NotFoundPageRoot,
  NotFoundText,
} from "./NotFoundPage.styles";
import { MainRoutes } from "../../router/MainRouter.types";
import { Col } from "../../designSystem/Grid";

const NotFoundPage = (): JSX.Element => {
  return (
    <NotFoundPageRoot>
      <NotFound404Text variant="h1">404</NotFound404Text>
      <Col gap="0.5rem">
        <NotFoundText variant="h2">{"Not Found"}</NotFoundText>
        <NotFoundText variant="h3">
          {"The content you are looking for does not exist"}
        </NotFoundText>
      </Col>
      <Link to={MainRoutes.MAIN}>
        <button>{"Go back home"}</button>
      </Link>
    </NotFoundPageRoot>
  );
};

export default NotFoundPage;
