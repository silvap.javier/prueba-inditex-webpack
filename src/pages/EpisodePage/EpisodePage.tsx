import clsx from "clsx";
import { EpisodePageRoot } from "./EpisodePage.styles";
import { useParams } from "react-router-dom";
import useGetPodcast from "../../hooks/useGetPodcast";
import PodcastCardDescription from "../../containers/PodcastCardDescription/PodcastCardDescription";
import { EpisodePageProps } from "./EpisodePage.types";
import EpisodeCard from "../../containers/EpisodeCard/EpisodeCard";

function EpisodePage({ className, ...rest }: EpisodePageProps): JSX.Element {
    const params = useParams();
    const podcastId = Number(params.podcastId);
    const episodeId = Number(params.episodeId);
    const { data } = useGetPodcast(podcastId);
    return (
        <EpisodePageRoot className={clsx("home-page", className)} {...rest}>
            <PodcastCardDescription podcast={data} />
            <EpisodeCard episodeId={episodeId} podcastId={podcastId} />
        </EpisodePageRoot>
    );
}

export default EpisodePage;
