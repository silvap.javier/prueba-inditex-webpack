export interface EpisodePageProps {
    className?: string;
    style?: React.CSSProperties;
}