import styled, { css } from "styled-components";
import BasePage from "../../layout/BasePage/BasePage";
import { Col } from "../../designSystem/Grid";

export const HomePageRoot = styled(BasePage)(
    () => css`
    `,
);

export const HomePageWrapper = styled(Col)(
    ({ theme }) => css`
        row-gap: ${theme.spacing["7"]};
        flex: 1;
    `,
);