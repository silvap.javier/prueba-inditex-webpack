import clsx from "clsx";
import { PodcastPageRoot } from "./PodcastPage.styles";
import { HomePageProps } from "./PodcastPage.types";
import { useParams } from "react-router-dom";
import useGetPodcast from "../../hooks/useGetPodcast";
import PodcastCardDescription from "../../containers/PodcastCardDescription/PodcastCardDescription";
import PodcastCardEpisodes from "../../containers/PodcastCardEpisodes/PodcastCardEpisodes";

function PodcastPage({ className, ...rest }: HomePageProps): JSX.Element {
    const params = useParams();
    const id = Number(params.id);
    const { data } = useGetPodcast(id);
    return (
        <PodcastPageRoot className={clsx("home-page", className)} {...rest}>
            <PodcastCardDescription podcast={data} />
            <PodcastCardEpisodes id={id} />
        </PodcastPageRoot>
    );
}

export default PodcastPage;
