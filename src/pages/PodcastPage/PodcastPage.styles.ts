import styled, { css } from "styled-components";
import DescriptionPage from "../../layout/DescriptionPage/DescriptionPage";
import { Col } from "../../designSystem/Grid";

export const PodcastPageRoot = styled(DescriptionPage)(
    () => css`
    `,
);

export const HomePageWrapper = styled(Col)(
    ({ theme }) => css`
        row-gap: ${theme.spacing["7"]};
        flex: 1;
    `,
);