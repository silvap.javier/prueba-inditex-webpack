export interface HomePageProps {
    className?: string;
    style?: React.CSSProperties;
}