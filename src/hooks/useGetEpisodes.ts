import { useQuery } from '@tanstack/react-query';
import { EpisodesApiResponse } from '../types/episodes.types';


const fetchEpisodes = async (id:number) => {
    const url = `https://itunes.apple.com/lookup?id=${id}&media=podcast&entity=podcastEpisode&limit=20`;
    const response = await fetch(`https://api.allorigins.win/get?url=${encodeURIComponent(url)}`)
    
    const data = await response.json();

    return data.contents ? JSON.parse(data.contents) : null;
};

const useGetEpisodes = (id:number) => {
  return  useQuery<EpisodesApiResponse>({ queryKey: ['getEpisodes',id], queryFn: () => fetchEpisodes(id)})
};

export default useGetEpisodes;
