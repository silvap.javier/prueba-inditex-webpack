import Typography from "../../components/Typography/Typography";
import { Card, CardBody, CardTitle } from "../../designSystem/Card";
import { PodcastCardEpisodesCountProps } from "./PodcastCardEpisodesCount.types";

function PodcastCardEpisodesCount({
  count,
}: PodcastCardEpisodesCountProps): JSX.Element {
  return (
    <Card>
      <CardBody variant="default">
        <CardTitle>
          <Typography variant="h4">{`Episodes : ${count}`}</Typography>
        </CardTitle>
      </CardBody>
    </Card>
  );
}

export default PodcastCardEpisodesCount;
