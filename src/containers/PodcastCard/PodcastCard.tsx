import { PodcastCardProps } from "./PodcastCard.types";
import { useNavigate } from "react-router-dom";
import { MainRoutes } from "../../router/MainRouter.types";
import getInfoPodcast from "../../utils/getInfoPodcast";
import {
  Card,
  CardText,
  CardTitle,
  CardImage,
  CardBody,
} from "../../designSystem/Card";

function PodcastCard({ podcast }: PodcastCardProps): JSX.Element {
  const navigate = useNavigate();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [image, title, author, description, id] = getInfoPodcast(podcast);
  return (
    <Card
      style={{
        width: "15rem",
        minHeight: "13rem",
        cursor: "pointer",
      }}
      onClick={() =>
        navigate(MainRoutes.VIEW_PODCAST.replace(":id", String(id)))
      }
    >
      <CardImage src={image} />
      <CardBody>
        <CardTitle>{title}</CardTitle>
        <CardText>{`Author : ${author}`}</CardText>
      </CardBody>
    </Card>
  );
}

export default PodcastCard;
