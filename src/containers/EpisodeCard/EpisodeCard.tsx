import { EpisodeCardProps } from "./EpisodeCard.types";
import { useState } from "react";
import useGetEpisodes from "../../hooks/useGetEpisodes";
import { EpisodeCardComponent, EpisodeCardRoot } from "./EpisodeCard.styles";
import {
  Card,
  CardBody,
  CardFooter,
  CardText,
  CardTitle,
} from "../../designSystem/Card";

function EpisodeCard({ podcastId, episodeId }: EpisodeCardProps): JSX.Element {
  const { data: episodes } = useGetEpisodes(podcastId);

  const episode = episodes?.results.find(
    (episode) => episode.trackId === episodeId
  );
  const [isPlaying, setIsPlaying] = useState(false);

  const togglePlay = () => {
    setIsPlaying(!isPlaying);
  };

  return (
    <EpisodeCardRoot>
      <EpisodeCardComponent>
        <CardBody variant="default">
          <CardTitle>{episode?.trackName}</CardTitle>
          <CardText>{episode?.description}</CardText>
        </CardBody>
        <CardFooter>
          <audio
            controls={true}
            autoPlay={false}
            onPlay={togglePlay}
            onPause={togglePlay}
          >
            <source src={episode?.episodeUrl} type="audio/mpeg" />
            Your browser does not support the audio element.
          </audio>
        </CardFooter>
      </EpisodeCardComponent>
    </EpisodeCardRoot>
  );
}

export default EpisodeCard;
