import { css, styled } from "styled-components";
import { Col } from "../../designSystem/Grid";
import { Card } from "../../designSystem/Card";

export const EpisodeCardRoot = styled(Col)(
    () => css`
        display: flex;
        flex-direction: column;
        gap: 1.5rem;
        width: 70%;
    `,
);

export const EpisodeCardComponent = styled(Card)(
    () => css`
        padding: 1rem;
    `,
);