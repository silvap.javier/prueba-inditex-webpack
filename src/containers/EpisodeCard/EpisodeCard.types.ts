export type EpisodeCardProps = {
    episodeId: number;
    podcastId: number;
};