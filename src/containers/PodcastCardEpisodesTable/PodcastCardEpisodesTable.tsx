import { PodcastCardEpisodesTableProps } from "./PodcastCardEpisodesTable.types";
import { getHeadersEpisodesList } from "../../utils/getHeadersEpisodesList";
import { getRowsEpisodesList } from "../../utils/getRowsEpisodesList";
import EpisodesTable from "../EpisodesTable/EpisodesTable";
import { Card, CardBody } from "../../designSystem/Card";

function PodcastCardEpisodesTable({
  episodes,
  podcastId,
}: PodcastCardEpisodesTableProps): JSX.Element {
  const headers = getHeadersEpisodesList();
  const rows = getRowsEpisodesList(episodes);
  return (
    <Card>
      <CardBody variant="default">
        <EpisodesTable headers={headers} rows={rows} podcastId={podcastId} />
      </CardBody>
    </Card>
  );
}

export default PodcastCardEpisodesTable;
