import { Result } from "../../types/episodes.types";

export type PodcastCardEpisodesTableProps = {
    episodes: Result[];
    podcastId: number;
};