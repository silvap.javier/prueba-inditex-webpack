export type FilterProps = {
    className?: string;
    searhTerm: string;
    setSearchTerm: (term: string) => void;
    count: number;
};