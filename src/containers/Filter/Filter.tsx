import clsx from "clsx";
import { FilterProps } from "./Filter.types";
import { FilterRoot, FilterWrapper } from "./Filter.styles";
import SearchBar from "../../components/SearchBar/SearchBar";
import Badge from "../../components/Badge/Badge";

function Filter({
  className,
  searhTerm,
  setSearchTerm,
  count,
}: FilterProps): JSX.Element {
  return (
    <FilterRoot className={clsx("Filter", className)}>
      <FilterWrapper gap="4">
        <Badge value={count} style={{ marginRight: "1rem" }} />
        <SearchBar
          value={searhTerm}
          onSearch={setSearchTerm}
          placeholder="Filter podcasts"
        />
      </FilterWrapper>
    </FilterRoot>
  );
}

export default Filter;
