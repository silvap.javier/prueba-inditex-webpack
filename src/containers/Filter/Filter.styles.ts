import { css, styled } from "styled-components";
import { Row } from "../../designSystem/Grid";

export const FilterRoot = styled(Row)(
    () => css`
        justify-content: flex-end;
    `,
);

export const FilterWrapper = styled(Row)(
    ({ theme }) => css`
        width: 30rem;
        justify-content: space-between;
        align-items: center;
    `,
);