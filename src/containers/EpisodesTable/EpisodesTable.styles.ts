import { css, styled } from "styled-components";
import { Table } from "../../designSystem/Table";

export const EpisodesTableRoot = styled(Table)(
    ({theme}) => css`
       a {
            color: ${theme.palette.primary};
        }
    `,
);