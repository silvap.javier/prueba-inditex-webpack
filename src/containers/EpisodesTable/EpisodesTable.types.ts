import { EpisodeInfo } from "../../utils/getRowsEpisodesList";

export interface EpisodesTableProps {
    className?: string;
    style?: React.CSSProperties;
    headers: string[];
    rows: EpisodeInfo[];
    podcastId: number;
}