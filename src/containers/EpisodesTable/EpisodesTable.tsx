import { EpisodesTableProps } from "./EpisodesTable.types";
import clsx from "clsx";
import { EpisodesTableRoot } from "./EpisodesTable.styles";
import { Link } from "react-router-dom";
import { MainRoutes } from "../../router/MainRouter.types";
import { EpisodeInfo } from "../../utils/getRowsEpisodesList";

function EpisodesTable({
  className,
  headers,
  rows,
  podcastId,
}: EpisodesTableProps): JSX.Element {
  const getLinkToEpisode = (episode: EpisodeInfo) => {
    return MainRoutes.VIEW_EPISODE.replace(
      ":podcastId",
      String(podcastId)
    ).replace(":episodeId", String(episode.id));
  };

  return (
    <EpisodesTableRoot className={clsx("table", className)}>
      <thead>
        <tr>
          {headers.map((header, index) => (
            <th key={index}>{header}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {rows.map((row, rowIndex) => {
          const filteredRow = Object.entries(row).filter(
            ([key]) => key !== "id"
          );

          return (
            <tr key={rowIndex}>
              {filteredRow.map(([, value], cellIndex) => (
                <td key={cellIndex}>
                  {cellIndex === 0 ? (
                    <Link to={getLinkToEpisode(rows[rowIndex])}>{value}</Link>
                  ) : (
                    value
                  )}
                </td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </EpisodesTableRoot>
  );
}

export default EpisodesTable;
