import { css, styled } from "styled-components";
import { Col } from "../../designSystem/Grid";
import { Card as BaseCard, CardImage as BaseCardImage,CardBody as BaseCardBody,CardFooter as BaseCardFooter} from "../../designSystem/Card";

export const Card = styled(BaseCard)(
    () => css`
        width: 15rem;
        min-height: 13rem;
        cursor: pointer;
    `,
);
export const CardImage = styled(BaseCardImage)(
    () => css`
    
    `,
);

export const CardBody = styled(BaseCardBody)(
    () => css`
        text-align: left;
        border-top: 1px solid #e0e0e0;
        padding: 2rem 0rem 2rem 2rem;
        cursor: pointer;
    `,
);

export const CardFooter = styled(BaseCardFooter)(
    () => css`
        text-align: left;
        border-top: 1px solid #e0e0e0;
        gap-column: 1rem;
        background-color: transparent;
        padding-top: 1.5rem;
    `,
);



export const CardFooterContent = styled(Col)(
    () => css`
        display: flex;
        flex-direction: column;
        gap: 0.5rem;
        overflow: hidden;
        position: relative;
    `,
);