import { css, styled } from "styled-components";

export const PodcastGridRoot = styled("div")(
    () => css`
        margin-top: 1rem;
    `,
);