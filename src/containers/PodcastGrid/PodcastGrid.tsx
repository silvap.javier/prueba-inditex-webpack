import Grid from "../../components/Grid/Grid";
import Toasts from "../../components/Toasts/Toasts";
import { Entry } from "../../types/podcasts.types";
import PodcastCard from "../PodcastCard/PodcastCard";
import { PodcastGridRoot } from "./PodcastGrid.styles";
import { PodcastGridProps } from "./PodcastGrid.types";

function PodcastGrid({ podcasts, isError }: PodcastGridProps): JSX.Element {
  const renderItem = (podcast: Entry) => {
    return <PodcastCard podcast={podcast} />;
  };

  return (
    <PodcastGridRoot>
      {isError ? (
        <Toasts
          title="Error"
          message="Failed to fetch podcasts"
          show={isError}
        />
      ) : (
        <Grid
          items={podcasts as unknown as Entry[]}
          renderItem={renderItem}
          count={4}
        />
      )}
    </PodcastGridRoot>
  );
}

export default PodcastGrid;
