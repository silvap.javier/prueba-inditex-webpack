import { Entry } from "../../types/podcasts.types";

export type PodcastGridProps = {
    podcasts: Entry[];
    isError: boolean;
};