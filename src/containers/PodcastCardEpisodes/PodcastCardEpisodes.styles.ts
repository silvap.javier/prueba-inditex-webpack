import { css, styled } from "styled-components";
import { Col } from "../../designSystem/Grid";

export const PodcastCardEpisodesRoot = styled(Col)(
    () => css`
        display: flex;
        flex-direction: column;
        gap: 1.5rem;
        width: 70%;
    `,
);