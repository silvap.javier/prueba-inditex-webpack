import { ReactNode } from "react";

export interface DescriptionPageProps {
    children: [ReactNode, ReactNode];
    className?: string;
    style?: React.CSSProperties;
}