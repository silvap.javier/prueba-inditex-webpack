import clsx from "clsx";
import { DescriptionPageProps } from "./DescriptionPage.types";
import { DescriptionPageRoot, DescriptionPageRow } from "./DescriptionPage.styles";

function DescriptionPage({ className, children, ...rest }: DescriptionPageProps) {
    return (
        <DescriptionPageRoot className={clsx("description-page", className)} {...rest}>
            <DescriptionPageRow>
                {children[0]}
                {children[1]}
            </DescriptionPageRow>
        </DescriptionPageRoot>
    );
}

export default DescriptionPage;
