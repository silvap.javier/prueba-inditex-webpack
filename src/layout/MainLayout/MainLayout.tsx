import { PropsWithChildren } from "react";
import BaseLayout from "../BaseLayout/BaseLayout";
import Header from "../../navigation/Header/Header";
import { Col } from "../../designSystem/Grid";

const MainLayout = ({ children }: PropsWithChildren<{}>): JSX.Element => (
  <BaseLayout>
    <Col style={{ width: "100%" }}>
      <Header title="Podcaster" />
      {children}
    </Col>
  </BaseLayout>
);

export default MainLayout;
