import styled, { css } from "styled-components";

export const BaseLayoutRoot = styled.main(
    () => css`
        flex: 1;
        display: flex;
    `,
);
