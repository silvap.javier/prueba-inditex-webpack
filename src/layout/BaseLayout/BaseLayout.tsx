import { BaseLayoutRoot } from "./BaseLayout.styles";
import { ReactNode } from "react";
import { CSSProperties } from "styled-components";
import clsx from "clsx";

export interface BaseLayoutProps {
  children?: ReactNode;
  className?: string;
  style?: CSSProperties;
}

export default function BaseLayout({
  className,
  ...rest
}: BaseLayoutProps): JSX.Element {
  return (
    <BaseLayoutRoot className={clsx("base-layout", className)} {...rest} />
  );
}
