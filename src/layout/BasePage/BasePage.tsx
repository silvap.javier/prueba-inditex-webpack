import { BasePageRoot } from "./BasePage.styles";
import clsx from "clsx";
import { BasePageProps } from "./BasePage.types";

function BasePage({ className, ...rest }: BasePageProps) {
  return <BasePageRoot className={clsx("base-page", className)} {...rest} />;
}

export default BasePage;
