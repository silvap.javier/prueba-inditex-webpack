import React, { ReactNode } from "react";
import styled from "styled-components";

export interface ToastProps {
  show: boolean;
  onClose: () => void;
  className?: string;
  children?: ReactNode;
}

export const Toast: React.FC<ToastProps> = ({
  show,
  onClose,
  className,
  children,
}) => {
  if (!show) return null;

  return (
    <StyledToastWrapper className={className}>
      <StyledToast>
        {children}
        <CloseButton onClick={onClose}>&times;</CloseButton>
      </StyledToast>
    </StyledToastWrapper>
  );
};

const StyledToastWrapper = styled.div`
  position: fixed;
  top: 20px;
  right: 20px;
  z-index: 9999;
`;

const StyledToast = styled.div`
  position: relative;
  max-width: 350px;
  min-width: 250px;
  background-color: #fff;
  border: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: 0.25rem;
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15);
  padding: 15px;
  margin-bottom: 15px;
`;

const CloseButton = styled.button`
  position: absolute;
  top: 5px;
  right: 10px;
  background: none;
  border: none;
  cursor: pointer;
  font-size: 20px;
  font-weight: bold;
  color: #000;
  padding: 0;
  outline: none;
`;

interface ToastHeaderProps {
  className?: string;
  children?: ReactNode;
}

export const ToastHeader: React.FC<ToastHeaderProps> = ({
  className,
  children,
}) => {
  return (
    <StyledToastHeader className={className}>{children}</StyledToastHeader>
  );
};

const StyledToastHeader = styled.div`
  font-size: 1rem;
  font-weight: 700;
`;

interface ToastBodyProps {
  className?: string;
  children?: ReactNode;
}

export const ToastBody: React.FC<ToastBodyProps> = ({
  className,
  children,
}) => {
  return <StyledToastBody className={className}>{children}</StyledToastBody>;
};

const StyledToastBody = styled.div`
  font-size: 0.875rem;
  padding-top: 0.5rem;
`;
