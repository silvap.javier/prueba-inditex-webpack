import React, { ReactNode, ChangeEvent, KeyboardEvent } from "react";
import styled from "styled-components";

interface FormControlProps {
  children: ReactNode;
  label?: string;
  className?: string;
}

export const FormControl: React.FC<FormControlProps> = ({
  children,
  label,
  className,
}) => {
  return (
    <StyledFormControl className={className}>
      {label && <Label>{label}</Label>}
      {children}
    </StyledFormControl>
  );
};

const StyledFormControl = styled.div`
  margin-bottom: 1rem;
`;

const Label = styled.label`
  display: inline-block;
  margin-bottom: 0.5rem;
  font-weight: 500;
`;

export interface InputProps {
  placeholder?: string;
  value?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onKeyPress?: (e: KeyboardEvent<HTMLInputElement>) => void;
  className?: string;
}

export const Input: React.FC<InputProps> = ({
  placeholder,
  value,
  onChange,
  onKeyPress,
  className,
}) => {
  return (
    <StyledInput
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      onKeyPress={onKeyPress}
      className={className}
    />
  );
};

const StyledInput = styled.input`
  display: block;
  width: 100%;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
`;
