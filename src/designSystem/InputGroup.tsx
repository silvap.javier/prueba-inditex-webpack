import React, { ReactNode } from "react";
import styled from "styled-components";

interface InputGroupProps {
  children: ReactNode;
  className?: string;
}

export const InputGroup: React.FC<InputGroupProps> = ({
  children,
  className,
}) => {
  return <StyledInputGroup className={className}>{children}</StyledInputGroup>;
};

const StyledInputGroup = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

interface AddonProps {
  children: ReactNode;
  position?: "left" | "right";
  className?: string;
}

export const Addon: React.FC<AddonProps> = ({
  children,
  position = "left",
  className,
}) => {
  return (
    <StyledAddon position={position} className={className}>
      {children}
    </StyledAddon>
  );
};

const StyledAddon = styled.div<AddonProps>`
  display: flex;
  align-items: center;
  padding: 0.375rem 0.75rem;
  border: 1px solid #ced4da;
  border-radius: ${(props) =>
    props.position === "left" ? "0.25rem 0 0 0.25rem" : "0 0.25rem 0.25rem 0"};
`;

interface InputProps {
  className?: string;
}

export const Input: React.FC<InputProps> = ({ className }) => {
  return <StyledInput className={className} />;
};

const StyledInput = styled.input`
  flex: 1;
  padding: 0.375rem 0.75rem;
  border: 1px solid #ced4da;
  border-radius: 0.25rem;
`;

// Example Usage:
// import { InputGroup, Addon, Input } from "./components/InputGroup";
