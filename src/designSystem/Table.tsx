import React, {
  ReactNode,
  TableHTMLAttributes,
  TdHTMLAttributes,
  ThHTMLAttributes,
} from "react";
import styled from "styled-components";

interface TableProps extends TableHTMLAttributes<HTMLTableElement> {
  children: ReactNode;
  className?: string;
}

export const Table: React.FC<TableProps> = ({
  children,
  className,
  ...rest
}) => {
  return (
    <StyledTable className={className} {...rest}>
      {children}
    </StyledTable>
  );
};

const StyledTable = styled.table`
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  border-collapse: collapse;

  th,
  td {
    padding: 0.75rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
  }

  thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
    text-align: left;
  }

  tbody + tbody {
    border-top: 2px solid #dee2e6;
  }
`;

interface TableHeadProps {
  children: ReactNode;
  className?: string;
}

export const TableHead: React.FC<TableHeadProps> = ({
  children,
  className,
}) => {
  return <thead className={className}>{children}</thead>;
};

interface TableBodyProps {
  children: ReactNode;
  className?: string;
}

export const TableBody: React.FC<TableBodyProps> = ({
  children,
  className,
}) => {
  return <tbody className={className}>{children}</tbody>;
};

interface TableRowProps {
  children: ReactNode;
  className?: string;
}

export const TableRow: React.FC<TableRowProps> = ({ children, className }) => {
  return <tr className={className}>{children}</tr>;
};

interface TableCellProps extends TdHTMLAttributes<HTMLTableCellElement> {
  children: ReactNode;
  className?: string;
  isHeader?: boolean;
}

export const TableCell: React.FC<TableCellProps> = ({
  children,
  className,
  isHeader = false,
  ...rest
}) => {
  return isHeader ? (
    <th className={className} {...rest}>
      {children}
    </th>
  ) : (
    <td className={className} {...rest}>
      {children}
    </td>
  );
};
