import React, { ReactNode, CSSProperties } from "react";
import styled from "styled-components";

interface ContainerProps {
  children: ReactNode;
  className?: string;
}

export const Container: React.FC<ContainerProps> = ({
  children,
  className,
}) => {
  return <StyledContainer className={className}>{children}</StyledContainer>;
};

const StyledContainer = styled.div`
  width: 100%;
  max-width: 1140px;
  margin: 0 auto;
`;

interface RowProps {
  children: ReactNode;
  className?: string;
  style?: CSSProperties;
  gap?: string;
}

export const Row: React.FC<RowProps> = ({
  children,
  className,
  style,
  gap,
}) => {
  return (
    <StyledRow className={className} style={{ ...style, gap }}>
      {children}
    </StyledRow>
  );
};

const StyledRow = styled.div<RowProps>`
  display: flex;
  flex-wrap: wrap;
  margin-left: -${(props) => props.gap || "0"};
  margin-right: -${(props) => props.gap || "0"};
  flex-direction: row;
  gap: ${(props) => props.gap || "0"};
  & > * {
    display: inline-block;
    flex: ${(props) => `0 0 calc(100% - ${props.gap || "0"})`};
    max-width: ${(props) => `calc(100% - ${props.gap || "0"})`};
  }
`;

interface ColProps {
  children: ReactNode;
  xs?: number;
  sm?: number;
  md?: number;
  lg?: number;
  xl?: number;
  className?: string;
  style?: CSSProperties;
  gap?: string;
}

export const Col: React.FC<ColProps> = ({
  children,
  xs,
  sm,
  md,
  lg,
  xl,
  className,
  style,
  gap,
}) => {
  const colWidth = {
    flex: `0 0 calc((100% - ${gap || "0"}) / 12 * ${
      xl || lg || md || sm || xs || 12
    })`,
    maxWidth: `calc((100% - ${gap || "0"}) / 12 * ${
      xl || lg || md || sm || xs || 12
    })`,
    ...style,
  };

  return (
    <StyledCol
      style={{ ...colWidth, margin: `0 ${gap || "0"}` }}
      className={className}
    >
      {children}
    </StyledCol>
  );
};

const StyledCol = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  box-sizing: border-box;
`;
