import React from "react";
import styled from "styled-components";

export interface BadgeProps {
  children?: React.ReactNode;
  variant?: "primary" | "secondary" | "success" | "danger" | "warning";
  className?: string;
  style?: React.CSSProperties; // Agregar la prop style
}

const getVariantColor = (variant: string): string => {
  switch (variant) {
    case "primary":
      return "#007bff";
    case "secondary":
      return "#6c757d";
    case "success":
      return "#28a745";
    case "danger":
      return "#dc3545";
    case "warning":
      return "#ffc107";
    default:
      return "#007bff";
  }
};

const StyledBadge = styled.span<BadgeProps>`
  display: inline-block;
  padding: 0.25em 0.4em;
  font-size: 75%;
  font-weight: 700;
  line-height: 1;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: 0.25rem;
  color: #fff;
  background-color: ${(props) => getVariantColor(props.variant || "primary")};
`;

const Badge: React.FC<BadgeProps> = ({
  children,
  variant,
  className,
  style,
}) => {
  return (
    <StyledBadge variant={variant} className={className} style={style}>
      {children}
    </StyledBadge>
  );
};

export default Badge;
