import React from "react";
import styled, { keyframes } from "styled-components";

interface SpinnerProps {
  variant?: "border" | "grow";
  size?: "sm" | "md" | "lg";
  className?: string;
}

const spinKeyframes = keyframes`
  to {
    transform: rotate(360deg);
  }
`;

const growKeyframes = keyframes`
  0% {
    transform: scale(0);
    opacity: 0;
  }
  50% {
    opacity: 1;
  }
  100% {
    transform: scale(1);
    opacity: 0;
  }
`;

const getSize = (size: string): string => {
  switch (size) {
    case "sm":
      return "1rem";
    case "lg":
      return "3rem";
    default:
      return "2rem";
  }
};

const StyledSpinner = styled.div<SpinnerProps>`
  display: inline-block;
  width: ${(props) => getSize(props.size || "md")};
  height: ${(props) => getSize(props.size || "md")};
  border: ${(props) =>
    props.variant === "border" ? "0.25em solid currentColor" : "none"};
  border-radius: 50%;
  background-color: currentColor;
  border-top-color: transparent;
  position: absolute;
  right: 1rem;
  animation: ${(props) =>
      props.variant === "border" ? spinKeyframes : growKeyframes}
    0.75s linear infinite;
`;

const Spinner: React.FC<SpinnerProps> = ({ variant, size, className }) => {
  return <StyledSpinner variant={variant} size={size} className={className} />;
};

export default Spinner;
