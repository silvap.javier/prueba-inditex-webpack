import React, { ReactNode, CSSProperties } from "react";
import styled, { css } from "styled-components";

interface CardProps {
  style?: CSSProperties;
  onClick?: () => void;
  className?: string;
  children?: ReactNode;
}

export const Card: React.FC<CardProps> = ({
  style,
  onClick,
  className,
  children,
}) => {
  return (
    <StyledCard style={style} onClick={onClick} className={className}>
      {children}
    </StyledCard>
  );
};

const StyledCard = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: 0.25rem;
  cursor: pointer;
  position: relative;
  box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
`;

interface CardImageProps {
  src: string;
  alt?: string;
  onClick?: () => void;
  style?: CSSProperties;
  variant?: "relative" | "absolute";
}

export const CardImage: React.FC<CardImageProps> = ({
  src,
  alt,
  onClick,
  style,
  variant = "absolute",
}) => {
  return (
    <StyledCardImage
      src={src}
      alt={alt}
      onClick={onClick}
      style={style}
      variant={variant}
    />
  );
};

const StyledCardImage = styled.img<{ variant: "relative" | "absolute" }>`
  width: 10rem;
  height: 10rem;
  border-radius: 50%;
  cursor: pointer; /* Add cursor pointer for image */

  ${(props) =>
    props.variant === "absolute" &&
    css`
      position: absolute;
      top: -5rem;
      left: 3rem;
    `}

  ${(props) =>
    props.variant === "relative" &&
    css`
      position: relative;
      margin: 0 auto;
      display: block;
      width: 14rem;
      height: 14rem;
      border-radius: 0;
      position: relative;
      padding: 1.25rem;
    `}
`;

interface CardBodyProps {
  className?: string;
  children?: ReactNode;
  onClick?: () => void;
  variant?: "card" | "default";
}

export const CardBody: React.FC<CardBodyProps> = ({
  className,
  children,
  onClick,
  variant = "card",
}) => {
  return (
    <StyledCardBody className={className} onClick={onClick} variant={variant}>
      {children}
    </StyledCardBody>
  );
};

const StyledCardBody = styled.div<{ variant: "card" | "default" }>`
  ${(props) =>
    props.variant === "card" &&
    css`
      padding-top: 6rem;
      text-align: center;
      cursor: pointer; /*
    `}

  ${(props) =>
    props.variant === "default" &&
    css`
      padding: 1.25rem;
      text-align: left;
    `}
`;

export const CardTitle: React.FC<{
  className?: string;
  children?: ReactNode;
}> = ({ className, children }) => {
  return <StyledCardTitle className={className}>{children}</StyledCardTitle>;
};

const StyledCardTitle = styled.h5`
  margin-bottom: 0.75rem;
`;

export const CardText: React.FC<{
  className?: string;
  children?: ReactNode;
}> = ({ className, children }) => {
  return <StyledCardText className={className}>{children}</StyledCardText>;
};

const StyledCardText = styled.p`
  margin-top: 0;
  margin-bottom: 1rem;
`;

export const CardFooter: React.FC<{
  className?: string;
  children?: ReactNode;
}> = ({ className, children }) => {
  return <StyledCardFooter className={className}>{children}</StyledCardFooter>;
};

const StyledCardFooter = styled.div`
  padding: 0.75rem 1.25rem;
  background-color: #f7f7f7;
  border-top: 1px solid rgba(0, 0, 0, 0.125);
  text-align: center;
`;
