import clsx from "clsx";
import { BadgeRoot } from "./Badge.styles";
import { BadgeProps } from "./Badge.types";

function Badge({ className, value, style }: BadgeProps): JSX.Element {
  return (
    <BadgeRoot className={clsx("badge", className)} style={style}>
      {value}
    </BadgeRoot>
  );
}

export default Badge;
