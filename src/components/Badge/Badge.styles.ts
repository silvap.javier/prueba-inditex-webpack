import { css, styled } from "styled-components";
import Badge from "../../designSystem/Badge";

export const BadgeRoot = styled(Badge)(
    ({ theme }) => css`
        background-color: ${theme.palette.primary};
        color: ${theme.palette.white};
        padding: ${theme.spacing["1"]};
        border-radius: ${theme.borderRadius};
        font-size: ${theme.fontSizes["base"]};
        width: 2rem;
        height: 1rem;
    `,
);