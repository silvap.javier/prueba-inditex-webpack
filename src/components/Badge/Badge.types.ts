import { BadgeProps as  BaseBadgeProps} from "../../designSystem/Badge";

export interface BadgeProps extends BaseBadgeProps {
    className?: string;
    style?: React.CSSProperties;
    value: number | string;
}