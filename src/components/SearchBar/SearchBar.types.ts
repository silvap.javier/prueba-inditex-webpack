import { InputProps } from "../../designSystem/FormControl";

export interface SearchBarProps extends Omit<InputProps, "prefix" | "defaultValue"> {
    placeholder?: string;
    onSearch: (value: string) => void;
    defaultValue?: string;
}
