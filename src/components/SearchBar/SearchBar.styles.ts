import { styled } from "styled-components";
import { Container } from "../../designSystem/Grid";
import { InputGroup } from "../../designSystem/InputGroup";

export const SearchContainer = styled(Container)`
  width: 24rem;
  padding: 0;
`;

export const StyledInputGroup = styled(InputGroup)`
  flex: 1;
`;