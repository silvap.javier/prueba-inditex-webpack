import clsx from "clsx";
import { TypographyRoot } from "./Typography.styles";
import { TypographyProps } from "./Typography.types";

function Typography({ className, children, variant }: TypographyProps): JSX.Element {
    return (
        <TypographyRoot className={clsx(variant, "typography", className)} as={variant}>
            {children}
        </TypographyRoot>
    );
}

export default Typography;
