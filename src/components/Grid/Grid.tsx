import { Col, Row } from "../../designSystem/Grid";
import { GridContainer } from "./Grid.styles";
import { GridProps } from "./Grid.types";

const Grid = <T,>({ items, count, renderItem }: GridProps<T>): JSX.Element => {
  return (
    <>
      {items && items.length > 0 ? (
        <GridContainer>
          {items.map(
            (item, index) =>
              index % count === 0 && (
                <Row key={index} style={{ marginTop: "6rem" }} gap="1">
                  {items.slice(index, index + count).map((item, subIndex) => (
                    <Col
                      key={subIndex}
                      xs={12}
                      sm={6}
                      md={3}
                      style={{ position: "relative" }}
                    >
                      {renderItem(item)}
                    </Col>
                  ))}
                </Row>
              )
          )}
        </GridContainer>
      ) : (
        <></>
      )}
    </>
  );
};

export default Grid;
