import { styled } from "styled-components";
import { Container } from "../../designSystem/Grid";

export const GridContainer = styled(Container)`
    padding-top: 20px;
`;