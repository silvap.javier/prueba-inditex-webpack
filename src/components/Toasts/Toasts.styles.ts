import { styled } from "styled-components";
import { Toast as BaseToast } from "../../designSystem/Toast";

export const ToastRoot = styled(BaseToast)`
  position: absolute;
  top: 0;
  right: 1rem;
`;