import { ToastProps } from "./Toasts.types";
import Typography from "../Typography/Typography";
import { ToastRoot } from "./Toasts.styles";
import { useState } from "react";
import { ToastBody, ToastHeader } from "../../designSystem/Toast";

function Toasts({ title, message, show, ...rest }: ToastProps): JSX.Element {
  const [showToast, setShowToast] = useState(show);
  return (
    <ToastRoot show={showToast} onClose={() => setShowToast(false)} {...rest}>
      <ToastHeader>
        <Typography variant="h6">{title}</Typography>
      </ToastHeader>
      <ToastBody>{message}</ToastBody>
    </ToastRoot>
  );
}

export default Toasts;
