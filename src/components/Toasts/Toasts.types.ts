import { ToastProps as ToastPropsProps } from "../../designSystem/Toast";

export interface ToastProps extends Omit<ToastPropsProps,"onClose"> {
    title: string;
    message?: string;
}