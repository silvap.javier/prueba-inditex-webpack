import { BrowserRouter, useRoutes } from "react-router-dom";
import { useMainRoutes } from "./MainRoutes";
import NotFoundPage from "../pages/NotFoundPage/NotFoundPage";
import React from "react";

const Routes = () => {
    const mainRoutes = useMainRoutes();
    return useRoutes([...mainRoutes, { path: "*", element: <NotFoundPage /> }]);
};

const Router = (): JSX.Element => {
    return (
        <BrowserRouter>
            <Routes />
        </BrowserRouter>
    );
};

export default Router;
