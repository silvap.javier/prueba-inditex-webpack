// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter as Router, useParams } from "react-router-dom";
import EpisodePage from "../../src/pages/EpisodePage/EpisodePage";
import * as useGetPodcastModule from "../../src/hooks/useGetPodcast";
import { EntryMock } from "../mock/Podcast.mock";
import { UseQueryResult } from "@tanstack/react-query";
import { Entry } from "../../src/types/podcasts.types";
import ConfigProvider from "../../src/theme/ConfigProvider";
import * as useGetEpisodesModule from "../../src/hooks/useGetEpisodes";
import { episodesApiResponseMock } from "../mock/Episodes.mock";
import { EpisodesApiResponse } from "../../src/types/episodes.types";

// Mockear useParams
jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useParams: jest.fn(),
}));

describe("EpisodePage", () => {
    let useGetPodcastMock: jest.SpyInstance;
    let useGetEpisodesMock: jest.SpyInstance;

    beforeEach(() => {
        useGetPodcastMock = jest.spyOn(useGetPodcastModule, "default").mockReturnValue({
            data: EntryMock,
            error: null,
            isError: false,
            isLoading: false,
            isSuccess: true,
            refetch: jest.fn(),
        } as unknown as UseQueryResult<Entry, Error>);

        useGetEpisodesMock = jest.spyOn(useGetEpisodesModule, "default").mockReturnValue({
            data: episodesApiResponseMock,
            error: null,
            isError: false,
            isLoading: false,
            isSuccess: true,
            refetch: jest.fn(),
        } as unknown as UseQueryResult<EpisodesApiResponse, Error>);
    });

    afterEach(() => {
        useGetPodcastMock.mockRestore();
    });

    test("renders EpisodePage correctly", () => {
        (useParams as jest.Mock).mockReturnValue({ podcastId: "1", episodeId: episodesApiResponseMock.results[0].trackId });

        const { getByText } = render(
            <Router>
                <ConfigProvider>
                    <EpisodePage />
                </ConfigProvider>
            </Router>,
        );

        expect(getByText("Podcast Name")).toBeInTheDocument();
        expect(getByText("Podcast summary")).toBeInTheDocument();
    });

    test("renders EpisodePage correctly - Details", async () => {
        (useParams as jest.Mock).mockReturnValue({ podcastId: "1", episodeId: episodesApiResponseMock.results[0].trackId });

        const { getByText } = render(
            <Router>
                <ConfigProvider>
                    <EpisodePage />
                </ConfigProvider>
            </Router>,
        );

        expect(getByText("Podcast Name")).toBeInTheDocument();
        expect(getByText("Podcast summary")).toBeInTheDocument();
        expect(getByText(episodesApiResponseMock.results[0].trackName)).toBeInTheDocument();
        expect(getByText(episodesApiResponseMock.results[0].description!)).toBeInTheDocument();
    });
});
