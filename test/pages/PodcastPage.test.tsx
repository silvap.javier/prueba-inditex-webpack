// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from "react";
import { render } from "@testing-library/react";
import { BrowserRouter as Router, useParams } from "react-router-dom";
import PodcastPage from "../../src/pages/PodcastPage/PodcastPage";
import * as useGetPodcastModule from "../../src/hooks/useGetPodcast";
import * as useGetEpisodesModule from "../../src/hooks/useGetEpisodes";
import { UseQueryResult } from "@tanstack/react-query";
import { Entry } from "../../src/types/podcasts.types";
import { EntryMock } from "../mock/Podcast.mock";
import ConfigProvider from "../../src/theme/ConfigProvider";
import { EpisodesApiResponse } from "../../src/types/episodes.types";
import { episodesApiResponseMock } from "../mock/Episodes.mock";
import moment from "moment";

// Mockear useParams
jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useParams: jest.fn(),
}));

describe("PodcastPage", () => {
    let useGetPodcastMock: jest.SpyInstance;
    let useGetEpisodesMock: jest.SpyInstance;

    beforeEach(() => {
        useGetPodcastMock = jest.spyOn(useGetPodcastModule, "default").mockReturnValue({
            data: EntryMock,
            error: null,
            isError: false,
            isLoading: false,
            isSuccess: true,
            refetch: jest.fn(),
        } as unknown as UseQueryResult<Entry, Error>);

        useGetEpisodesMock = jest.spyOn(useGetEpisodesModule, "default").mockReturnValue({
            data: episodesApiResponseMock,
            error: null,
            isError: false,
            isLoading: false,
            isSuccess: true,
            refetch: jest.fn(),
        } as unknown as UseQueryResult<EpisodesApiResponse, Error>);
    });

    afterEach(() => {
        useGetPodcastMock.mockRestore();
    });

    test("renders PodcastPage correctly - Details", () => {
        (useParams as jest.Mock).mockReturnValue({ id: "123" });
        const { getByText } = render(
            <Router>
                <ConfigProvider>
                    <PodcastPage />
                </ConfigProvider>
            </Router>,
        );
        expect(getByText("Podcast Name")).toBeInTheDocument();
        expect(getByText("Podcast summary")).toBeInTheDocument();
    });

    test("renders PodcastPage correctly - Episodes", () => {
        (useParams as jest.Mock).mockReturnValue({ id: "123" });
        const { getByText } = render(
            <Router>
                <ConfigProvider>
                    <PodcastPage />
                </ConfigProvider>
            </Router>,
        );
        expect(getByText("Podcast Name")).toBeInTheDocument();
        expect(getByText("Podcast summary")).toBeInTheDocument();
        expect(getByText("Episodes : " + episodesApiResponseMock.resultCount)).toBeInTheDocument();
        expect(getByText(episodesApiResponseMock.results[0].trackName)).toBeInTheDocument();
        expect(getByText(moment(episodesApiResponseMock.results[0].releaseDate).format("DD/MM/YYYY"))).toBeInTheDocument();
        expect(getByText(moment.utc(episodesApiResponseMock.results[0].trackTimeMillis).format("HH:mm:ss"))).toBeInTheDocument();
    });
});
