import { Entry } from "../../src/types/podcasts.types";

export const EntryMock:  Partial<Entry> = {
    "im:name": { label: "Podcast Name" },
    "im:image": [{ label: "Image URL", attributes: { height: "100" } }],
    summary: { label: "Podcast summary" },
    rights: { label: "Rights" },
    title: { label: "Podcast Title" },
    id: { label: "ID", attributes: { "im:id": "123456" } },
    "im:artist": { label: "Artist Name", attributes: { href: "https://example.com/artist" } },
};