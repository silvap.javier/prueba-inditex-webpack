// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from "react";
import { render } from "@testing-library/react";
import Typography from "../../../src/components/Typography/Typography";
import ConfigProvider from "../../../src/theme/ConfigProvider";

describe("Typography component", () => {
    test("renders correctly with children and variant", () => {
        // Define los datos necesarios para la prueba
        const variant = "h1";
        const className = "custom-class";
        const children = "Test";

        // Renderiza el componente
        const { getByText } = render(
            <ConfigProvider>
                <Typography variant={variant} className={className}>
                    {children}
                </Typography>
            </ConfigProvider>,
        );

        // Verifica que el componente se haya renderizado correctamente con los children y el variant
        const typographyElement = getByText(children);
        expect(typographyElement).toBeInTheDocument();
        expect(typographyElement).toHaveClass(variant);
        expect(typographyElement).toHaveClass("typography");
        expect(typographyElement).toHaveClass(className);
    });

    test("renders correctly without className", () => {
        // Define los datos necesarios para la prueba
        const variant = "h2";
        const children = "Test";

        // Renderiza el componente
        const { getByText } = render(
            <ConfigProvider>
                <Typography variant={variant}>{children}</Typography>{" "}
            </ConfigProvider>,
        );

        // Verifica que el componente se haya renderizado correctamente sin className
        const typographyElement = getByText(children);
        expect(typographyElement).toBeInTheDocument();
        expect(typographyElement).toHaveClass(variant);
        expect(typographyElement).toHaveClass("typography");
    });
});
