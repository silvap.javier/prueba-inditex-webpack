// eslint-disable-next-line @typescript-eslint/no-unused-vars
import React from "react";
import { render } from "@testing-library/react";
import Grid from "../../../src/components/Grid/Grid";

describe("Grid component", () => {
    test("renders items correctly", () => {
        const items = [
            { id: 1, name: "Item 1" },
            { id: 2, name: "Item 2" },
            { id: 3, name: "Item 3" },
        ];

        const renderItem = jest.fn((item) => <div>{item.name}</div>);
        const { getByText } = render(<Grid items={items} count={2} renderItem={renderItem} />);

        expect(getByText("Item 1")).toBeInTheDocument();
        expect(getByText("Item 2")).toBeInTheDocument();
        expect(getByText("Item 3")).toBeInTheDocument();
    });

    test("renders nothing when items array is empty", () => {
        const { container } = render(<Grid items={[]} count={2} renderItem={() => null} />);
        expect(container.firstChild).toBeNull();
    });
});
