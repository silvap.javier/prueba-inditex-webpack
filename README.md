# Prueba Técnica Senior Frontend

Este Proyecto fue creado como alternativa a la primera version :

https://gitlab.com/silvap.javier/prueba-tecnica-inditex

En este primer repositorio se puede observar, comit por comit, el progreso en el armado de la arquitectura de la aplicacion.

En la actual repositorio se realizaron los siguiente cambios :

Se reemplazdo el create-react-app por un armado con webpack.
Se reemplazando a react-boostrap por un propio design-system

## Técnologias empleadas

Para la implementacion de las solusion se utilizaron las siguientes técnologias :

- react 18.2.0 - Framework de implementacion de SPA
- typescript 5.4.5 - Para tipado del código
- styled-components 6.1.8 - Para customizacion de los components
- react-router-dom 6.22.3 - Para la navegacion y enrutado
- @tanstack/react-query 5.29.2 - Para el manejo de las peticiones fetch y cache
- zustand 4.5.2 - Manejo de estados (En la aplicación el manejo del loading)
- moment 2.30.1 - Manejo de fechas de los episodios y la duración del mismo
- prettier 3.2.5 - Formateo y limpieza de codigo
- eslint 8.50.0 - Analisis del codigo posibles errores
- jest 29.7.0 - Desarrollo de Unit Tests y Component Tests

## Scripts

Se crearon los siguientes scrips :

### `yarn start`

Corre la aplicacion de modo development en local : [http://localhost:3000](http://localhost:3000)

### `yarn test`

Lanzar test con jest visualizacion el coverage.

### `yarn build`

Compilacion para Produccion
